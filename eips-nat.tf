resource "aws_eip" "nat1" {
  tags = {
    Name = "eip-1-tf"
  }
  depends_on = [aws_internet_gateway.igw]
}

resource "aws_eip" "nat2" {
  tags = {
    Name = "eip-2-tf"
  }
  depends_on = [aws_internet_gateway.igw]
}

resource "aws_nat_gateway" "nat1" {
  allocation_id = aws_eip.nat1.id
  subnet_id     = aws_subnet.public_ap_southeast_1a.id

  tags = {
    Name = "nat-gw-1"
  }

  depends_on = [aws_internet_gateway.igw]
}

resource "aws_nat_gateway" "nat2" {
  allocation_id = aws_eip.nat2.id
  subnet_id     = aws_subnet.public_ap_southeast_1b.id

  tags = {
    Name = "nat-gw-2"
  }

  depends_on = [aws_internet_gateway.igw]
}
