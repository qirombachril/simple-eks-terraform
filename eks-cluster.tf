resource "aws_iam_role" "role_cluster" {
  name = "eks-cluster-role-tf"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "policy_attachment" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.role_cluster.name
}

resource "aws_eks_cluster" "cluster" {
  name     = "eks-cluster-tf"
  version  = "1.26"
  role_arn = aws_iam_role.role_cluster.arn

  vpc_config {
    subnet_ids = [
      aws_subnet.private_ap_southeast_1a.id,
      aws_subnet.private_ap_southeast_1b.id,
      aws_subnet.public_ap_southeast_1a.id,
      aws_subnet.public_ap_southeast_1b.id
    ]
  }

  depends_on = [aws_iam_role_policy_attachment.policy_attachment]
}
